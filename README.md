# templates

A simple tool that pre-populates ~/Templates with basic files, translated
into the user's language.

## Files created

- `Basic File.txt`: A basic text document
- `Office/`: A directory for LibreOffice files
	- `Text Document.odt`: Writer
	- `Spreadsheet.ods`: Calc
	- `Presentation.odp`: Impress
	- `Drawing.odg`: Draw
