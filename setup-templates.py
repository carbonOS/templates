#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
from collections import namedtuple

def _(message): return message # We're just using _() to mark strings for xgettext

File = namedtuple("File", ["name", "ext", "filename"])
Dir = namedtuple("Dir", ["name", "children"])

# Define the files to create
TEMPLATES = [
    # TRANSLATORS: A blank file with a .txt extension
    File(name=_("Basic File"), ext="txt", filename="basic.txt"),
    # TRANSLATORS: Directory containing blank LibreOffice documents
    Dir(name=_("Office"), children=[
        # TRANSLATORS: A LibreOffice Writer document, but name it in a generic way
        File(name=_("Text Document"), ext="odt", filename="writer.odt"),
        # TRANSLATORS: A LibreOffice Calc document, but name it in a generic way
        File(name=_("Spreadsheet"), ext="ods", filename="calc.ods"),
        # TRANSLATORS: A LibreOffice Impress document, but name it in a generic way
        File(name=_("Presentation"), ext="odp", filename="impress.odp"),
        # TRANSLATORS: A LibreOffice Draw document, but name it in a generic way
        File(name=_("Drawing"), ext="odg", filename="draw.odg"),
    ]),
]

##############################################################################################

import gi
gi.require_version("GLib", "2.0")
from gi.repository import GLib

import gettext
from pathlib import Path
import shutil

# Define some constants (filled in by Meson)
PROJECT_NAME = "@PROJECT_NAME@"
LOCALEDIR = "@LOCALEDIR@"
DATADIR = "@DATADIR@"

# Set up translations
translation = gettext.translation(PROJECT_NAME, LOCALEDIR, None, fallback=True)
translate = translation.gettext
language = translation.info().get("language", "C")
old_lang_path = Path(GLib.get_user_config_dir()) / "templates.locale"
if old_lang_path.is_file():
    old_lang = old_lang_path.read_text().strip()
    translation_old = gettext.translation(PROJECT_NAME, LOCALEDIR, [old_lang], fallback=True)
    translate_old = translation_old.gettext
else:
    translate_old = translate

# Find the paths we're reading from / writing to
source_dir = Path(DATADIR) / PROJECT_NAME
dest_dir = Path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_TEMPLATES))

# Do the copying
def process_file(path: Path, file: File):
    source = source_dir / file.filename
    dest = path / f"{translate(file.name)}.{file.ext}"
    dest_old = path / f"{translate_old(file.name)}.{file.ext}"

    # Don't overwrite any files
    if dest.is_file():
        print(f"File {dest} already exists. Skipping")
        return

    # Check to see if we just need to rename
    if dest_old.is_file():
        print(f"Renaming {dest_old} -> {dest}")
        dest_old.rename(dest)
        return

    # Not renaming, so let's copy the file from DATADIR
    print(f"Installing {source} -> {dest}")
    dest.parent.mkdir(parents=True, exist_ok=True)
    shutil.copyfile(source, dest)

def process_dir(path: Path, children: list[File]):
    for child in children:
        if isinstance(child, File):
            process_file(path, child)
        else:
            # Check to see if we need to rename the directory
            dirpath = path / translate(child.name)
            dirpath_old = path / translate_old(child.name)
            if dirpath_old.is_dir() and dirpath_old != dirpath:
                if dirpath.is_dir():
                    print(f"Directory {dirpath} already exists. NOT renaming {dirpath_old}")
                    dirpath = dirpath_old # Don't rename, but still recurse under old name
                else:
                    print(f"Renaming {dirpath_old} -> {dirpath}")
                    dirpath_old.rename(dirpath)

            # Recurse into the directory
            process_dir(dirpath, child.children)

process_dir(dest_dir, TEMPLATES)

# Mark down the locale we're operating in
print(f"Writing '{language}' -> {old_lang_path}")
old_lang_path.write_text(language)
